#include "MainWindow.h"
#include "ui_MainWindow.h"

/**
 * @brief MainWindow::MainWindow
 *  The MainWindow ties together multiple elements of the BG4LM program. It creates the widgets that
 *  contain configuration and the video as well as manages button presses and events for them and the
 *  facial detection thread.
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->mainToolBar->hide();
    ui->statusBar->hide();

    /* Prepare for QSettings */
    QCoreApplication::setOrganizationName(APP_ORG);
    QCoreApplication::setOrganizationDomain(ORG_DOMAIN);
    QCoreApplication::setApplicationName(APP_NAME);
    m_savedFilePathKey = KEY_APP + QString("/") + KEY_LOOPVIDEO + QString("/") + KEY_FILE;

    /* Window Style */
    setWindowTitle(APP_TITLE);
    setMaximumSize(CONF_WIDTH,CONF_HEIGHT);
    setMinimumSize(CONF_WIDTH,CONF_HEIGHT);
    resize(maximumSize());

    m_configScreen = new ConfigScreen(this);

    /* facial detection will be done in another thread */
    m_facialDetector = new FacialDetector();
    m_detectionThread = new QThread;

    /*the loop screen plays the video*/
    m_loopScreen = new LoopScreen(this);
    m_screenOffTimer = new QTimer(this);

    m_facialDetector->moveToThread(m_detectionThread);

    connect(m_detectionThread,SIGNAL(started()),m_facialDetector,SLOT(process()));
    connect(m_facialDetector,SIGNAL(finished()),m_detectionThread,SLOT(quit()));
    connect(m_facialDetector,SIGNAL(finished()),m_detectionThread,SLOT(deleteLater()));

    connect(m_facialDetector,SIGNAL(noFaceDetected()),m_loopScreen,SLOT(waitForStop()));
    connect(m_facialDetector,SIGNAL(faceDetected()),m_loopScreen,SLOT(waitForStart()));

    connect(m_configScreen,SIGNAL(fileFound()),this,SLOT(existingFileFound()));
    connect(m_configScreen,SIGNAL(exitClicked()),this,SLOT(exitButtonClicked()));
    connect(m_configScreen,SIGNAL(startClicked()),this,SLOT(startButtonClicked()));

    connect(m_loopScreen,SIGNAL(done()),this,SLOT(loopScreenDone()));

    m_clearSettingsHotKey = new QShortcut(QKeySequence("ctrl+e"),this);
    connect(m_clearSettingsHotKey,SIGNAL(activated()),this,SLOT(clearSettings()));

    setCentralWidget(m_configScreen);

    if(loadSettings())
        m_configScreen->startButtonClicked();

}

MainWindow::~MainWindow()
{
    delete ui;

    delete m_loopScreen;
    delete m_configScreen;
    delete m_facialDetector;

    //delete m_detectionThread;
    delete m_screenOffTimer;

}

/**
 * @brief MainWindow::saveSettings
 *  Use platform independent methods of saving data. Windows will use the registry.
 * @return if saved or not
 */
bool MainWindow::saveSettings()
{
    QSettings settings;
    QString filePath = m_configScreen->fileLine()->text();
    if(!filePath.isEmpty())
        if(QFile(filePath).exists())
            settings.setValue(m_savedFilePathKey,filePath);
        else
            return false;
    else
        return false;

    settings.sync();
    return true;
}

/**
 * @brief MainWindow::loadSettings
 * Use platform independent methods to load data. Windows will use the registry.
 * @return
 */
bool MainWindow::loadSettings()
{    
    QSettings settings;
    settings.sync();
    QString filePath;

    if(settings.contains(m_savedFilePathKey)){
        if(!(filePath = settings.value(m_savedFilePathKey).toString()).isEmpty()){
            if(QFile(filePath).exists()){
                m_configScreen->fileLine()->setText(filePath);
                m_configScreen->setFilePath(filePath);
            }else{ return false;}
        }else{return false;}
    }else{
        Screen::error(TITLE_NOSAVE,TXT_NOSAVE);
        return false;
    }
    return true;
}

void MainWindow::clearSettings()
{
    QSettings settings;
    if(settings.contains(m_savedFilePathKey))
        settings.remove(m_savedFilePathKey);

    settings.clear();
    Screen::error(TITLE_CLEARED,INFO_CLEARED);
}

/**
 * @brief MainWindow::existingFileFound
 * When a file is known to exist this small one liner helps save it and is handy
 * because it's a slot.
 */
void MainWindow::existingFileFound()
{
    m_configScreen->setFilePath(m_configScreen->fileLine()->text());
    return;
}

/**
 * @brief MainWindow::startButtonClicked
 * Save settings, begin the video if applicable and then proceed to start the facial
 * detection thread.
 */
void MainWindow::startButtonClicked()
{
    /* The file is now assumed to exist because of earlier checks */

    if(!saveSettings())
        Screen::error(TITLE_ERROR,TXT_UNSAVED);

    m_configScreen->layout()->addWidget(m_loopScreen);
    m_loopScreen->show();
    m_loopScreen->setFilePath(m_configScreen->fileLine()->text());
    m_loopScreen->loadMedia();
    m_loopScreen->startVideo();
    m_loopScreen->stopVideo();

    m_facialDetector->setAbort(false);
    while(!m_detectionThread->isRunning())
        m_detectionThread->start();

    return;
}

/**
 * @brief MainWindow::exitButtonClicked
 * Quit the FacialDetector thread and close the main application
 */
void MainWindow::exitButtonClicked()
{

    while(m_detectionThread->isRunning()){
        m_facialDetector->abort();
        m_detectionThread->quit();
    }
    this->close();
}

/**
 * @brief MainWindow::loopScreenDone
 * The exit hotkey was pressed for the LoopScreen and some cleaning up will be done
 */
void MainWindow::loopScreenDone()
{
    /* Config Screen is garbage collected when taken by Qt*/
    m_loopScreen->videoWidget()->hide();
    m_loopScreen->hide();
    m_configScreen->layout()->removeWidget(m_loopScreen);

    setCentralWidget(m_configScreen);

    m_configScreen->fileLine()->setText(m_loopScreen->filePath());
    existingFileFound();

}



