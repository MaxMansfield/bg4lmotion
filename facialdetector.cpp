#include "facialdetector.h"

/**
 * @brief FacialDetector::FacialDetector
 *  The FacialDetector class works well with LoopScreen to deliver media capabilites based on facial
 *  detection. Either way the signals faceDetected() and noFaceDetected() can be used for interesting effects.
 */
FacialDetector::FacialDetector()
{
    /* No abort on 1st run */
    m_abort = false;

    /*Get camera and frame size */
    m_webcam = cv::VideoCapture(0);
    m_webcam.set(CV_CAP_PROP_FPS, 10);

    m_frameWidth = m_webcam.get(CV_CAP_PROP_FRAME_WIDTH);
    m_frameHeight = m_webcam.get(CV_CAP_PROP_FRAME_HEIGHT);
    m_frameArea = m_frameWidth * m_frameHeight;


    /* initialize HaarCascades */

    std::string haarFile = QString(QDir::currentPath() + FACE_HAAR_F).toStdString();
    if(m_haarFaceFrontal.empty())
        if(!m_haarFaceFrontal.load(haarFile))
            Screen::error(TITLE_ERROR,ERR_FACE_HAAR_F);

    /* reserve vector to boost speed */
    m_defaultFaces.reserve(24);
    m_altTreeFaces.reserve(24);
    m_profilesPresent.reserve(24);
}

/**
 * @brief FacialDetector::process
 *  This function is meant to be called as a worker process in a separate thread from the GUI.
 *  it is still possible to call this process from the same thread but results will be less desirable.
 *  This method loops continuously grabbing frames from the local webcam until abort() is called.
 */
void FacialDetector::process()
{
    m_abort = false;

    if(!m_webcam.isOpened()){
        Screen::error(TITLE_ERROR,ERR_NOCAM);
        abort();
        return;
    }

    while(!m_abort && m_webcam.isOpened()){
        bool status = m_webcam.read(m_originalFrame);
        if(!status){
            Screen::error(TITLE_ERROR,ERR_READFRAME);
            abort();
            break;
        }

        status = false;
        m_haarFaceFrontal.detectMultiScale(m_originalFrame,
                                           m_defaultFaces,
                                           FACE_SCALE);

        for(int i = 0; i < m_defaultFaces.size(); i++)
            if(m_defaultFaces[i].area() > (m_frameArea / 64))
                status = true;

        if(!status)
            emit noFaceDetected();
        else
            emit faceDetected();

        // Exit on ctrl+c key
        if (cv::waitKey(30) == ASCII_ETX || m_abort)
            break;
    }


    while(m_webcam.isOpened())
       m_webcam.release();

    emit finished();
}

/**
 * @brief FacialDetector::abort
 *  Sets abort to true, exiting the process() loop
 */
void FacialDetector::abort()
{
    m_abort = true;
    return;
}

/**
 * @brief FacialDetector::setAbort
 *  Allows finer control of abort status.
 * @param abort - if the program should abort
 */
void FacialDetector::setAbort(bool abort)
{
    m_abort = abort;
}







