#-------------------------------------------------
#
# Project created by QtCreator 2015-01-04T05:13:18
#
#-------------------------------------------------

QT       += core gui
QT       += multimedia multimediawidgets


RESOURCES     += \
    icons.qrc

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BG4LMotion
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    configscreen.cpp \
    screen.cpp \
    facialdetector.cpp \
    loopscreen.cpp

HEADERS  += mainwindow.h \
    configscreen.h \
    screen.h \
    facialdetector.h \
    loopscreen.h

FORMS    += mainwindow.ui

RC_FILE = data/image/logos/logo.rc

INCLUDEPATH += include/
INCLUDEPATH += $$PWD/lib
INCLUDEPATH += $$PWD/staticlib

DEPENDPATH += $$PWD/lib
PRE_TARGETDEPS += $$PWD/lib/opencv_core2410.lib
PRE_TARGETDEPS += $$PWD/lib/opencv_objdetect2410.lib
PRE_TARGETDEPS += $$PWD/lib/opencv_highgui2410.lib
PRE_TARGETDEPS += $$PWD/lib/opencv_imgproc2410.lib

LIBS += -L$$PWD/lib/ -lopencv_core2410
LIBS += -lopencv_objdetect2410
LIBS +=  -lopencv_highgui2410
LIBS +=  -lopencv_imgproc2410



LIBS += -lopencv_contrib2410 \
-lopencv_stitching2410 \
-lopencv_nonfree2410 \
-lopencv_superres2410 \
-lopencv_ts2410 \
-lopencv_videostab2410 \
-lopencv_gpu2410 \
-lopencv_legacy2410 \
-lopencv_ml2410 \
-lopencv_calib3d2410 \
-lopencv_photo2410 \
-lopencv_video2410 \
-lopencv_features2d2410

LIBS += -L$$PWD/staticlib/ \
-llibjasper \
-lIlmImf \
-llibtiff \
-llibpng \
-llibjpeg \
-lopencv_flann2410 \
-lzlib \
-lVfw32 \
-lmsvfw32 \
-lComCtl32 \
-lGdi32

RESOURCES +=






