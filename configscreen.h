#ifndef CONFIGSCREEN_H
#define CONFIGSCREEN_H

#include "screen.h"
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QShortcut>

#define TXT_MIME  tr("MP4 (*.mp4);;All Files (*.*)")
#define TXT_FIND  tr("Find")
#define TXT_START tr("Start")
#define TXT_SAVE  tr("Save")
#define TXT_EXIT  tr("Exit")

#define TITLE_OPEN   tr("Open a video file to play.")

#define STYLE_LABEL "QLabel { font-weight: bold; font-size: 12px;}"

#define LABEL_FILE  tr("Select a video to play when motion is detected.")


class ConfigScreen : public Screen
{
    Q_OBJECT

public:
    explicit ConfigScreen(QWidget* parent);
    ~ConfigScreen();

    QLineEdit *fileLine() const;
    void setFileLine(QLineEdit *fileLine);


    QPushButton *startButton() const;
    void setStartButton(QPushButton *startButton);

    void setFilePath(QString filePath);

signals:
    void startClicked();
    void exitClicked();
    void fileFound();

public slots:
    int startButtonClicked();
    int exitButtonClicked();
    int findButtonClicked();

private:
    QWidget* m_parent;

    QLineEdit*  m_fileLine;
    QPushButton* m_fileButton;
    QPushButton* m_startButton;
    QPushButton* m_exitButton;

    QLabel*  m_fileLabel;
};

#endif // CONFIGSCREEN_H
