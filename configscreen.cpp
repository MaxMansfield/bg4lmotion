#include "configscreen.h"


/* No File Error */
#define err_nof()       error(TITLE_ERROR,QString(ERR_NOFILE) + "You should go back and fix the file path.")
#define err_blank()     error(TITLE_BLANK,ERR_BLANK);


/**
 * @brief ConfigScreen::ConfigScreen
 *  ConfigScreen is the initial screen which a user will see. This screen holds all the buttons and potential actions from startup.
 *  from this screen you may open a file dialog to choose a file, start the video via a LoopScreen instance and exit.
 * @param parent - Used for hierarchical placement
 */
ConfigScreen::ConfigScreen(QWidget* parent) : Screen(parent)
{
    m_parent = parent;

    m_fileLabel = new QLabel(LABEL_FILE,this);
    m_fileLabel->setStyleSheet(STYLE_LABEL);
    m_mainLayout->addWidget(m_fileLabel);

    m_fileLine = new QLineEdit(this);
    m_fileButton = new QPushButton(TXT_FIND,this);
    connect(m_fileButton,SIGNAL(clicked()),this,SLOT(findButtonClicked()));

    m_formLayout->addRow(m_fileButton,m_fileLine);
    m_mainLayout->addLayout(m_formLayout);

    m_startButton = new QPushButton(TXT_SAVE,this);
    connect(m_startButton,SIGNAL(clicked()),this,SLOT(startButtonClicked()));

    m_exitButton = new QPushButton(TXT_EXIT,this);
    connect(m_exitButton,SIGNAL(clicked()),this,SLOT(exitButtonClicked()));

    m_endLayout->addWidget(m_exitButton);
    m_endLayout->addWidget(m_startButton);

    m_mainLayout->addLayout(m_endLayout);
}

ConfigScreen::~ConfigScreen()
{
    delete m_startButton;
    delete m_exitButton;
    delete m_fileButton;
    delete m_fileLabel;
    delete m_fileLine;
}

/**
 * @brief ConfigScreen::startButtonClicked
 *  This slot validates text in the file select box as a real file before emiting a signal to be
 *  picked up by a parent slot
 * @return
 *  either success or the value of the last button clicked on an error message box ('OK','CANCEL','ETC')
 */
int ConfigScreen::startButtonClicked()
{
    m_filePath = m_fileLine->text();
    if(m_filePath.isEmpty())
        return err_blank();


    if(!QFile(m_filePath).exists())
        return err_nof();


    m_startButton->setText(TXT_START);

    emit startClicked();
    return EXIT_SUCCESS;
}

/**
 * @brief ConfigScreen::exitButtonClicked
 *  This slot closes the widget and emits a signal to notify parent widgets for any changes.
 * @return 0
 */
int ConfigScreen::exitButtonClicked()
{ 
    emit exitClicked();
    this->close();
    return 0;
}

/**
 * @brief ConfigScreen::findButtonClicked
 *  This slot allows a file dialog box to be opened. The selected file is analyzed for existance and
 *  a signal is emitted that a valid file has been found
 * @return
 */
int ConfigScreen::findButtonClicked()
{
    m_fileLine->setText(QFileDialog::getOpenFileName(this,TITLE_OPEN,
                                                     QDir::homePath(),
                                                     TXT_MIME));
    /* If the file doesn't exist and the line has info then error */
    if(!(QFile(m_fileLine->text()).exists())){
        if(!m_fileLine->text().isEmpty())
            return err_nof();
        else
            return ENOENT;
    }

    m_filePath = m_fileLine->text();
    m_startButton->setEnabled(true);

    if(!m_filePath.isEmpty())
        m_startButton->setText(TXT_START);

    emit fileFound();

    return EXIT_SUCCESS;
}


/**
 * @brief Getters and Setters
 */

QPushButton *ConfigScreen::startButton() const
{
    return m_startButton;
}

void ConfigScreen::setStartButton(QPushButton *startButton)
{
    m_startButton = startButton;
}

void ConfigScreen::setFilePath(QString filePath)
{
    m_fileLine->setText(filePath);
    m_filePath = filePath;
}

QLineEdit* ConfigScreen::fileLine() const
{
    return m_fileLine;
}

void ConfigScreen::setFileLine(QLineEdit *fileLine)
{
    m_fileLine = fileLine;
}









