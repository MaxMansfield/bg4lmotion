# Bath and Granite 4 Less Motion  

######VER. 1.0 ######
BG4LMotion is a multithreaded program which detects human faces on a webcam and plays a given MP4 according to whether or not there is a face present.

### Installation ###
1. Download the .zip folder from the [downloads page](https://bitbucket.org/MaxMansfield/bg4lmotion/downloads).
2. Extract the folder and copy the BG4LMotion folder to `C:\Program Files\`
3. Create a desktop shortcut. Right Click BG4LMotion.exe select `send to` and then `Desktop (create shortcut)`
4. Enable at boot. Paste the desktop shortcut into `C:\Users\Max\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup` but after changing the username.

### Operation ###
1. When you first open BG4LM it will prompt you for a file.
2. Select a file with the dialog box and click `start`.
3. Now the processing has begun and the video is playing fullscreen and will pause after 8 seconds of no motion. Move the mouse to the bottom of the screen until it is hidden.
5. When you open BG4LM from now on it will automatically enter this mode and begin playing that video. Press `CTRL+E` to erase the currently saved file.
4. you can press `CTRL+C` to exit the video and go back to the file dialog, from there you can choose another file.

### Information ###
The Bath and Granite 4 Less Motion system is a 64 bit, multithreaded facial detection program written in C++ using the Qt5 64bit framework and the OpenCV2 libraries.
##### Qt Based GUI #####
*  x64 bit
* Compiled with MSVC 2012 and OpenGL
* Dynamically linked

##### OpenCV2 #####
* x64 bit
* HaarCascade based detection
* Detection done in separate thread from GUI and Video looping
* Dynamic and static linking