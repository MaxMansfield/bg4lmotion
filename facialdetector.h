#ifndef FACIALDETECTOR_H
#define FACIALDETECTOR_H

#include "screen.h"
#include <QObject>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#define ERR_NOCAM       "No camera could be detected."
#define ERR_READFRAME   "Unable to read the next frame from the webcam."

#define ERR_FACE_HAAR_F "The frontal facial HaarCascade could not be loaded."
#define ERR_FACE_HAAR_P "The profile facial HaarCascade could not be loaded."

#define FACE_HAAR_F    "/data/haarcascades/haarcascade_frontalface_default.xml"
#define FACE_HAAR_F0   "/data/haarcascades/haarcascade_frontalface_alt_tree.xml"

#define FACE_HAAR_P    "/data/haarcascades/haarcascade_profileface.xml"
#define FACE_SCALE     1.45f
#define PROFILE_SCALE  2.0f

class FacialDetector : public QObject
{
    Q_OBJECT
public:
    explicit FacialDetector();
    void setAbort(bool abort);

public slots:
    void process();
    void abort();

signals:
    void finished();
    void faceDetected();
    void noFaceDetected();

private:
    cv::CascadeClassifier m_haarFaceFrontal;
    cv::Mat m_originalFrame;
    cv::VideoCapture m_webcam;

    std::vector<cv::Rect> m_defaultFaces;
    std::vector<cv::Rect> m_altTreeFaces;
    std::vector<cv::Rect> m_profilesPresent;

    double m_frameWidth;
    double m_frameHeight;
    double m_frameArea;
    bool m_abort;

};

#endif // FACIALDETECTOR_H
