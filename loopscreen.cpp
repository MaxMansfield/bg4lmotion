#include "loopscreen.h"


/**
 * @brief LoopScreen::LoopScreen
 *  LoopScreen facilitates playing of media based on slots it has being triggered.
 * @param parent -hierarchical ConfigScreen Parent
 */
LoopScreen::LoopScreen(QWidget* parent) : Screen(parent)
{

    m_badFrameCount = 0;
    m_goodFrameCount = 0;
    isPlaying = false;

    m_videoWidget = new QVideoWidget;
    m_videoWidget->setPalette(QPalette(QPalette::Window,Qt::black));
    m_mainLayout->addWidget(m_videoWidget);
    m_videoWidget->setVisible(false);

    m_player = new QMediaPlayer(this);
    m_playlist = new QMediaPlaylist(m_player);
    m_playlist->setPlaybackMode(QMediaPlaylist::Loop);
    m_player->setPlaylist(m_playlist);
    m_player->setVideoOutput(m_videoWidget);

    m_exitHotKey = new QShortcut(QKeySequence("ctrl+c"),m_videoWidget);
    connect(m_exitHotKey,SIGNAL(activated()),this,SLOT(closeVideo()));

    m_screenOffTimer = new QTimer(this);
    connect(m_screenOffTimer,SIGNAL(timeout()),this,SLOT(stopVideo()));

}

LoopScreen::~LoopScreen()
{
    delete m_player;
//    delete m_playlist;
//    delete m_videoWidget;
    delete m_exitHotKey;
    delete m_screenOffTimer;
}


/**
 * @brief LoopScreen::startVideo
 *  Start the video from the beginning
 */
void LoopScreen::startVideo()
{
    if(!isPlaying){
        isPlaying = true;
        m_videoWidget->setFullScreen(true);
        m_videoWidget->showFullScreen();
        m_player->setVolume(100);
        m_player->play();
    }
}

/**
 * @brief LoopScreen::stopVideo
 *  Stop the video
 */
void LoopScreen::stopVideo()
{
    if(isPlaying){
        isPlaying = false;
        m_player->setVolume(0);
        m_player->stop();
    }
}

/**
 * @brief LoopScreen::loadMedia
 *  Make sure that a video is loaded properly
 */
void LoopScreen::loadMedia()
{
    if(!m_playlist->children().empty())
        m_playlist->removeMedia(0);

    m_playlist->addMedia(QUrl::fromLocalFile(m_filePath));
    return;
}


/**
 * @brief LoopScreen::waitForStop
 *  Waits for 11 continous frames of no faces then starts the
 *  timer to stop the video. If the video is close to then end
 *  then it waits for the end of the video before stopping
 */
void LoopScreen::waitForStop()
{
    qint64 time = TIMER_LEN;
    bool atEnd;

    /* If the position is less than the duration minus the timer delay
     * then shorten the delay to that length and skip checking frameSize()
     */
    if((atEnd= m_player->position() >= (m_player->duration() - TIMER_LEN)))
        time = (m_player->duration() - TIMER_LEN);

    if(m_badFrameCount++ >= 10 || atEnd){
        m_badFrameCount = 0;

        if(!m_screenOffTimer->isActive())
            m_screenOffTimer->start(time);

    }
    m_goodFrameCount = 0;
}

/**
 * @brief LoopScreen::waitForStart
 *  Waits for 2 consecutive frames of faces before starting the video from
 *  the beginning.
 */
void LoopScreen::waitForStart()
{
    if(m_goodFrameCount++ >= 1 && m_badFrameCount <= m_goodFrameCount){
        m_goodFrameCount = 0;
        m_screenOffTimer->stop();
        startVideo();
    }

    m_badFrameCount = 0;
}

/**
 * @brief LoopScreen::closeVideo
 * Signal that the looping process is complete
 */
void LoopScreen::closeVideo()
{
    stopVideo();
    emit done();
}

/**
 * @brief Getters and Setters
 */
bool LoopScreen::getPlaying() const
{
    return isPlaying;
}

void LoopScreen::setPlaying(bool value)
{
    isPlaying = value;
}

QMediaPlaylist *LoopScreen::playlist() const
{
    return m_playlist;
}

void LoopScreen::setPlaylist(QMediaPlaylist *playlist)
{
    m_playlist = playlist;
}

QMediaPlayer *LoopScreen::player() const
{
    return m_player;
}

void LoopScreen::setPlayer(QMediaPlayer *player)
{
    m_player = player;
}

QVideoWidget *LoopScreen::videoWidget() const
{
    return m_videoWidget;
}

void LoopScreen::setVideoWidget(QVideoWidget *videoWidget)
{
    m_videoWidget = videoWidget;
}



