
#include "screen.h"

/**
 * @brief Screen::Screen
 *  Provides a laout foundation for GUI screens, can later be set as a central widget
 *  or used with other layouts.
 * @param parent
 */
Screen::Screen(QWidget *parent) : QWidget(parent)
{
    m_mainLayout = new QVBoxLayout(this);
    m_formLayout = new QFormLayout(this);
    m_endLayout = new QHBoxLayout(this);

    m_screenComplete = false;
}

Screen::~Screen()
{
    delete m_mainLayout;
//    delete m_formLayout;
//    delete m_endLayout;
}


/**
 * @brief Screen::error
 *  Static error handling for anything that includes Screen. Optionally takes files
 * @param txt
 *  The text to display as the window title
 * @param infoTxt
 *  Informative text that is the body of the error
 * @param errFile
 *  An optional file for error handling
 * @return - the button press of the message dialog
 */
int Screen::error(const QString &txt, const QString &infoTxt, const QFile *errFile)
{
    QString info;
    if(errFile != nullptr){
        info = infoTxt + "\n";
        info += QString(TXT_MORE);
        info += "\n\tError: " + errFile->errorString() + "\n\tCode: " + errFile->error() + "\n";
    } else { info = infoTxt; }

    return QMessageBox::warning(NULL,txt,info,QMessageBox::Ok);
}

/**
 * @brief Getters and Setters
 *
 */
void Screen::setMainLayout(QVBoxLayout *mainLayout)
{
    m_mainLayout = mainLayout;
}

QVBoxLayout *Screen::mainLayout() const
{
    return m_mainLayout;
}
QString Screen::filePath() const
{
    return m_filePath;
}

void Screen::setFilePath(QString filePath)
{
    m_filePath = filePath;
}

