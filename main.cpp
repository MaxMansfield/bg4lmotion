#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(true);
    MainWindow w;
    w.setWindowIcon(QIcon(":/Icons/Logo/Logo"));
    w.setAttribute(Qt::WA_QuitOnClose);
    w.show();

    /* Let the app close the detection thread before exiting */
    QObject::connect(&a, SIGNAL(aboutToQuit()), &w,SLOT(exitButtonClicked()));

    a.exec();
    return a.exit(0);
}
