#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QThread>
#include <QTimer>

#include "configscreen.h"
#include "loopscreen.h"
#include "facialdetector.h"

#define CONF_WIDTH  320
#define CONF_HEIGHT 120

#define APP_NAME   tr("B&G 4 Less Display System")
#define APP_ORG    tr("Bath and Granite 4 Less")
#define APP_GROUP  tr("MainWindow")

#define ORG_DOMAIN tr("http://www.bathandgranite4less.com/")

#define APP_TITLE tr("Display Configuration")

#define TITLE_CLEARED "Settings cleared"
#define INFO_CLEARED  "The settings have been cleared. Next time the program is run you will be prompted for a file."
#define TXT_UNSAVED   "Unable to save the location of the video."
#define TXT_UNLOADED  "Unable to load the location of the video."


#define KEY_APP   "BG4LM"
#define KEY_LOOPVIDEO "LoopVideo"
#define KEY_FILE  "FileName"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /* Saving and Loading will be handled by xml sheets */
    bool saveSettings();
    bool loadSettings();


public slots:
    void existingFileFound();
    void startButtonClicked();
    void exitButtonClicked();
    void clearSettings();
    void loopScreenDone();

private:
    void switchScreen(const Screens& nextScreen);
    Ui::MainWindow *ui;


    ConfigScreen*   m_configScreen;
    LoopScreen*     m_loopScreen;
    FacialDetector* m_facialDetector;



    QSettings     m_settings;
    QString       m_savedFilePathKey;

    QThread*      m_detectionThread;
    QTimer*       m_screenOffTimer;
    QShortcut*    m_clearSettingsHotKey;

};

#endif // MAINWINDOW_H
