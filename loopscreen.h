#ifndef LOOPSCREEN_H
#define LOOPSCREEN_H

#include "screen.h"
#include "facialdetector.h"

#include  <QShortcut>
#include <QTimer>
#include <QMultimedia>
#include <QVideoWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>

#define TIMER_LEN 8000

class LoopScreen : public Screen
{
    Q_OBJECT
public:
    LoopScreen(QWidget *parent);
    ~LoopScreen();

    void loadMedia();

    bool getPlaying() const;
    void setPlaying(bool value);

    QMediaPlayer *player() const;
    void setPlayer(QMediaPlayer *player);

    QMediaPlaylist *playlist() const;
    void setPlaylist(QMediaPlaylist *playlist);

    QVideoWidget *videoWidget() const;
    void setVideoWidget(QVideoWidget *videoWidget);

signals:
    void videoStopped();
    void videoStarted();
    void clearPressed();
    void done();

public slots:
    void waitForStop();
    void waitForStart();
    void closeVideo();
    void startVideo();
    void stopVideo();

private:
    bool isPlaying;
    long long unsigned int m_badFrameCount;
    long long unsigned int m_goodFrameCount;

    QMediaPlayer* m_player;
    QMediaPlaylist* m_playlist;
    QVideoWidget* m_videoWidget;


    QShortcut* m_exitHotKey;
    QShortcut* m_stopHotKey;
    QShortcut* m_startHotKey;
    QShortcut*    m_clearSettingsHotKey;

    QTimer* m_screenOffTimer;


    unsigned long long int m_lastPosition;

};

#endif // LOOPSCREEN_H
