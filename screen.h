#ifndef SCREEN_H
#define SCREEN_H

#include <QWidget>
#include <QMessageBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QFile>
#include <errno.h>

#define TITLE_ERROR  tr("There has been an error.")
#define TITLE_NOSAVE tr("Cannot locate any saved settings.")
#define TITLE_BLANK  tr("The file line is blank.")

#define ERR_NOFILE    tr("The file specified does not exist.\n")
#define ERR_OPEN      tr("Unable to open the settings file to save the video location.")
#define ERR_XMLFORMAT tr("Unable to load the settings file because it is empty or incorrectly formatted.\nPlease generate a new one.")
#define ERR_BLANK     tr("No file was specified. Click the find button to select a video file first.")

#define TXT_NOSAVE  tr("No saved video file could be found. Continue to select and save one.")
#define TXT_MORE    tr("More information is below: \n")

/* ctrl-c */
#define ASCII_ETX 3

/*ctrl-d */
#define ASCII_EOT 4

enum Screens {
    CONFIG_SCREEN,
    LOOPAV_SCREEN,
    EXIT_SCREEN
};

class Screen : public QWidget
{
    Q_OBJECT
public:
    explicit Screen(QWidget *parent);
    ~Screen();

    QVBoxLayout *mainLayout() const;
    void setMainLayout(QVBoxLayout *mainLayout);
    static int error(const QString& txt, const QString& infoTxt,const QFile* errFile = NULL );

    QString filePath() const;
    void setFilePath(QString filePath);

protected:
    QString  m_filePath;

    QVBoxLayout* m_mainLayout;
    QFormLayout* m_formLayout;
    QHBoxLayout* m_endLayout;

    bool m_screenComplete;
};

#endif // SCREEN_H
